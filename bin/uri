#!/usr/bin/env python
"""URI encode/decode strings

Usage:
    uri <method> [<file>...]"""

from typing import Callable

import os
import sys
import urllib.parse

methods: dict[str, Callable[[str], str]] = {
    "-e": urllib.parse.quote,
    "-q": urllib.parse.quote,
    "-d": urllib.parse.unquote,
    "-u": urllib.parse.unquote,
}

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print(__doc__, file=sys.stderr)
        sys.exit(os.EX_USAGE)
    method = methods.get(sys.argv.pop(1))
    if method is None:
        print(__doc__, file=sys.stderr)
        sys.exit(os.EX_USAGE)
    for line in sys.argv[1:] or sys.stdin:
        print(method(line.removesuffix("\n")))
